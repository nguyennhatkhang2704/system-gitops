## Set up Google Kubernetes Engine
### 1. Enable Workload identity
- [Google Cloud CLI](./cli/README.md)
- [Terraform](./terraform/README.md)
### 2. Create Service Accounts
- Crossplane
  - Follow instructions here and update:
    - KSA name
    - GSA name
    - GCP role
- External Secrets