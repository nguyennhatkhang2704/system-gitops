## Setup GKE Workload Identity Cluster 
### Enable workload identity on existing GKE Cluster
```
gcloud container clusters update <GKE CLUSTER NAME> --zone <GKE CLUSTER ZONE> --workload-pool=<GCP PROJECT ID>.svc.id.goog
```
### Modify an existing node pool to enable GKE_METADATA
1. List node pools to identify the node-pool name in the cluster
```
gcloud container node-pools list --cluster=<GKE CLUSTER NAME> --zone <GKE CLUSTER ZONE> --project <GCP PROJECT ID>
```
2. Update node pools with workload-metadata
```
gcloud container node-pools update CLUSTER_NAME_NODE_POOL_NAME --cluster=<GKE CLUSTER NAME> --zone <GKE CLUSTER ZONE> --workload-metadata-from-node=GKE_METADATA
```
3. Describe an existing node pool to verify the **workloadMetadataConfig**
```
gcloud container node-pools describe CLUSTER_NAME_NODE_POOL_NAME --cluster=<GKE CLUSTER NAME> --zone <GKE CLUSTER ZONE>
```
## Setup authentication on GKE workload
### Create a Google Cloud IAM Service Account
```
gcloud iam service-accounts create gitops-crossplane \
--project <GCP PROJECT ID>
```
### Grant the necessary permissions to the IAM Service Account
```
gcloud projects add-iam-policy-binding <GCP PROJECT ID>  \
--member serviceAccount:gitops-crossplane@<GCP PROJECT ID>.iam.gserviceaccount.com \
--role roles/storage.admin \
--role roles/compute.admin
``` 
### Create K8S Serviceaccount
```
kubectl create serviceaccount gitops-crossplane --namespace crossplane-system
```
### Bind Google Cloud IAM Service Account to K8S Serviceaccount
#### Rolebinding between GSA and KSA (Project scope)
```
gcloud iam service-accounts add-iam-policy-binding \
 gitops-crossplane@<GCP PROJECT ID>.iam.gserviceaccount.com \
--role  roles/iam.workloadIdentityUser \
--member "serviceAccount:<GCP PROJECT ID>.svc.id.goog[crossplane-system/gitops-crossplane]" \
--project <GCP PROJECT ID>
```
#### Bind the Kubernetes Service Account to the IAM Service Account (Cluster scope)
```
kubectl annotate serviceaccount \ upbound-provider-gcp-storage-392e0dc39543 iam.gke.io/gcp-service-account=gitops-crossplane@<GCP PROJECT ID>.iam.gserviceaccount.com -n crossplane-system
```
### Usage
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-app
spec:
  replicas: 1
  selector:
    matchLabels:
      app: my-app
  template:
    metadata:
      labels:
        app: my-app
    spec:
      serviceAccountName: gitops-crossplane
      containers:
      - name: my-app
        image: google/cloud-sdk:alpine
        command: ["gsutil", "ls", "gs://my-example-bucket"]
```