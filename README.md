## Components Introduction
1. FluxCD
2. Secret Management
3. Crossplane
## Structure of git repository
1. Monorepo for Devops Teams
```
├── infrastructure
│   ├── base
│   ├── production 
│   └── staging
└── clusters
    ├── production
    └── staging
```
## Setup on Google Kubernetes Engine
### 1. [Setup Cluster](./cloud/google/README.md)
### 2. [Create Service Accounts](./cloud/google/README.md)
- Crossplane
- External Secrets
### 3. Install Components
### 3.1. Crossplane
### [Install Crossplane GCP](https://docs.crossplane.io/v1.15/getting-started/provider-gcp)
1. Install the Crossplane Helm chart 
2. Install the GCP provider
3. Configurations Upbound Official GCP Provider to authentication
- [Workload identity](https://marketplace.upbound.io/providers/upbound/provider-family-gcp/v1.0.2/docs/configuration)
4. Create a ProviderConfig
5. Create a managed resource

### Troubleshooting
1. Get the name of your current ProviderRevision of this provider
```
kubectl get providers.pkg.crossplane.io <PROVIDER_GCP> -o jsonpath="{.status.currentRevision}"
```
2. Get ClusterRoleBinding
```
kubectl get ClusterRoleBinding crossplane:provider:<PROVIDER_GCP><REVISION>:system -o yaml
```
3. Get Provider
```
kubectl get providers.pkg.crossplane.io
```
4. Get ProviderConfig
```
kubectl get ProviderConfig
```
5. Get DeploymentRuntimeConfig
```
kubectl get DeploymentRuntimeConfig
```
6. Get Kustomization
```
kubectl -n flux-system get Kustomization
```
### Concepts
1. Provider
- Install a Provider:
- Configure a Provider:
  - Providers have two different types of configurations:
    - **DeploymentRuntimeConfigs** (Controller configurations) that change the settings of the Provider pod running inside the Kubernetes cluster
    - **Provider configurations** that change settings used when communicating with an external provider
----
### Refferer
- [Upbound Marketplace](https://marketplace.upbound.io)
- [Upbound Provider GCP](https://marketplace.upbound.io/providers/upbound/provider-family-gcp/v1.0.2/docs)
- [Google Cloud Storarge Bucket locations](https://cloud.google.com/storage/docs/locations)
- [Google Cloud Storarge Storage classes ](https://cloud.google.com/storage/docs/storage-classes)